all: shellcode.bin execover execover2

execover: execover.c Makefile
	unset LD_RUN_PATH; musl-gcc -O2 -Wall -o $@ $< -static
	sstrip $@

execover2: execover2.c Makefile
	gcc -O2 -Wall -Werror -o $@ $<

shellcode.bin: shellcode.asm
	nasm -f bin -o $@ $<

clean:
	rm -f execover execover2 shellcode.bin
