bits 64

section .text

shellcode_replace:
	syscall
	mov rax, 231 ; NR_exit_group
	mov rdi, 1
	syscall

shellcode_close:
	; rdi (file descriptor)
	mov rax, 3 ; NR_close
	syscall
	inc edi
	cmp edi, esi
	jne short shellcode_close
	int3

