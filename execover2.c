#define _GNU_SOURCE
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <errno.h>
#include <err.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <sys/ptrace.h>
#include <sys/user.h>
#include <sys/syscall.h>

static const unsigned char
shellcode_replace[] = {
	0x0f, 0x05, // syscall
	0xb8, 0xe7, 0x00, 0x00, 0x00, // mov rax, NR_exit_group
	0xbf, 0x01, 0x00, 0x00, 0x00, // mov rdi, 1
	0x0f, 0x05, // syscall
};

static const unsigned char
shellcode_close[] = {
	0xb8, 0x03, 0x00, 0x00, 0x00, // mov rax, NR_close
	0x0f, 0x05, // syscall
	0xff, 0xc7, // inc edi
	0x39, 0xf7, // cmp edi, esi
	0x75, 0xf3, // jne shellcode_close
	0xcc, // int3
};

static long allocate_env(pid_t pid, long rsp);
static long allocate_argv(pid_t pid, long rsp, char **argv);
static long allocate_string(pid_t pid, long rsp, const char *s);

#define ptrace_getregs(pid, regs) ptrace(PTRACE_GETREGS, (pid), NULL, (regs))
#define ptrace_setregs(pid, regs) ptrace(PTRACE_SETREGS, (pid), NULL, (regs))
static void ptrace_write(pid_t pid, long addr, const void *code, size_t code_size);
static void ptrace_read(pid_t pid, long addr, void *code, size_t code_size);
static void ptrace_cont(pid_t pid);

static long find_code(pid_t pid);

int main(int argc, char **argv)
{
	struct user_regs_struct regs, old_regs;
	long addr;

	if (argc < 3)
		return 1;

	pid_t pid = atoi(argv[1]);
	argv += 2;

	// attach process
	if (ptrace(PTRACE_ATTACH, pid, NULL, NULL))
		err(1, "Error attaching program");

	// check process is stopped
	int status;
	if (waitpid(pid, &status, WUNTRACED) != pid)
		errx(1, "waitpid failed");

	// set option EXEC to catch executions!
	if (ptrace(PTRACE_SETOPTIONS, pid, NULL, PTRACE_O_TRACEEXEC))
		err(1, "Setting ptrace options");

	if (ptrace_getregs(pid, &regs))
		err(1, "Error getting registries");
	
	// allocate array env (null ended!) and fill
	long envp = regs.rsp = allocate_env(pid, regs.rsp);

	// allocate argv and fill
	long argvp = regs.rsp = allocate_argv(pid, regs.rsp, argv);

	// allocate path and fill
	long pathp = regs.rsp = allocate_string(pid, regs.rsp, argv[0]);

	// find place to override
	addr = find_code(pid);

	// copy shell code
	ptrace_write(pid, addr, shellcode_replace, sizeof(shellcode_replace));

	// set regs syscall(path, argv, env)
	regs.rax = __NR_execve;
	regs.rdi = pathp;
	regs.rsi = argvp;
	regs.rdx = envp;
	regs.rip = addr;
	ptrace_setregs(pid, &regs);
	ptrace_cont(pid); // XXX check it's an exec

	ptrace_getregs(pid, &old_regs);

	// find new place to override
	addr = find_code(pid);

	// save code
	unsigned char old_code[sizeof(shellcode_close)];
	ptrace_read(pid, addr, old_code, sizeof(shellcode_close));

	// copy shell code to close files
	ptrace_write(pid, addr, shellcode_close, sizeof(shellcode_close));

	regs = old_regs;
	regs.rdi = 3;
	regs.rsi = 1024;
	regs.rip = addr;
	ptrace_setregs(pid, &regs);

	// execute shellcode, wait success
	ptrace_cont(pid);

	// restore code
	ptrace_write(pid, addr, old_code, sizeof(shellcode_close));

	// restore registers
	ptrace_setregs(pid, &old_regs);

	// good, now exit (program will continue)

	return 0;
}

long find_code(pid_t pid)
{
	char filename[64];
	char line[1024];
	long addr = 0;
	char perms[8];
	sprintf(filename, "/proc/%d/maps", pid);
	FILE *f = fopen(filename, "r");
	if (!f)
		err(1, "fopen");
	while (fgets(line, sizeof(line), f) != NULL) {
		if (sscanf(line, "%lx-%*x %7s %*s ", &addr, perms) != 2)
			continue;

		if (strstr(perms, "xp") != NULL)
			break;
	}
	fclose(f);
	return addr;
}

void ptrace_read(pid_t pid, long addr, void *code, size_t code_size)
{
	long *ptr = (long *) code;
	size_t readed;

	for (readed = 0; readed + sizeof(long) <= code_size; readed += sizeof(long)) {
		errno = 0;
		long word = ptrace(PTRACE_PEEKTEXT, pid, addr + readed, NULL);
		if (word == -1 && errno != 0)
			err(1, "ptrace(PTRACE_PEEKTEXT)");
		*ptr++ = word;
	}
	if (readed < code_size) {
		union {
			long word;
			unsigned char bytes[sizeof(long)];
		} u;
		errno = 0;
		u.word = ptrace(PTRACE_PEEKTEXT, pid, addr + readed, NULL);
		if (u.word == -1 && errno != 0)
			err(1, "ptrace(PTRACE_PEEKTEXT)");
		memcpy(ptr, u.bytes, code_size - readed);
	}
}

void ptrace_write(pid_t pid, long addr, const void *code, size_t code_size)
{
	size_t written;

	for (written = 0; written + sizeof(long) <= code_size; written += sizeof(long)) {
		long word;
		memcpy(&word, (const unsigned char*) code + written, sizeof(word));
		errno = 0;
		word = ptrace(PTRACE_POKETEXT, pid, addr + written, word);
		if (word == -1 && errno != 0)
			err(1, "ptrace(PTRACE_POKETEXT)");
	}
	if (written < code_size) {
		union {
			long word;
			unsigned char bytes[sizeof(long)];
		} u;
		errno = 0;
		u.word = ptrace(PTRACE_PEEKTEXT, pid, addr + written, NULL);
		if (u.word == -1 && errno != 0)
			err(1, "ptrace(PTRACE_PEEKTEXT)");
		memcpy(u.bytes, (const unsigned char*) code + written, code_size - written);
		ptrace(PTRACE_POKETEXT, pid, addr + written, u.word);
	}
}

static long allocate_env(pid_t pid, long rsp)
{
	// read environ from /proc
	const size_t max_env_size = 1024 * 1024;
	void *env = malloc(max_env_size);

	// read entire environment file into memory
	char filename[64];
	sprintf(filename, "/proc/%d/environ", pid);
	FILE *f = fopen(filename, "r");
	if (!f)
		err(1, "fopen");
	size_t readed = fread(env, 1, max_env_size, f);
	fclose(f);

	// copy to destination process
	rsp -= readed;
	rsp &= -8lu;
	const long env_addr = rsp;
	ptrace_write(pid, rsp, env, readed);

	// write NULL terminator
	long addr = 0;
	rsp -= sizeof(long);
	ptrace_write(pid, rsp, &addr, sizeof(addr));

	// write all environment pointers
	char *p = (char *) env + readed;
	while (p != env) {
		p = memrchr(env, 0, p - 1 - (char *) env);
		if (!p)
			p = env;
		else
			++p;
		addr = env_addr + (p - (char *) env);
		rsp -= sizeof(long);
		ptrace_write(pid, rsp, &addr, sizeof(addr));
	}

	free(env);
	return rsp;
}

static long allocate_string(pid_t pid, long rsp, const char *s)
{
	size_t len = strlen(s) + 1;

	// copy to destination process
	rsp -= len;
	rsp &= -8lu;
	ptrace_write(pid, rsp, s, len);

	return rsp;
}

static long allocate_argv(pid_t pid, long rsp, char **argv)
{
	unsigned num;
	long rsp_strings = rsp;
	for (num = 0; argv[num]; ++num) {
		size_t len = strlen(argv[num]) + 1;
		rsp -= len;
		ptrace_write(pid, rsp, argv[num], len);
	}
	rsp &= -8lu;

	// write NULL terminator
	long addr = 0;
	rsp -= sizeof(long);
	ptrace_write(pid, rsp, &addr, sizeof(addr));

	// write argv
	rsp -= sizeof(long) * num;
	for (num = 0; argv[num]; ++num) {
		size_t len = strlen(argv[num]) + 1;
		rsp_strings -= len;
		addr = rsp_strings;
		ptrace_write(pid, rsp + num * sizeof(long), &addr, sizeof(addr));
	}

	return rsp;
}

static void ptrace_cont(pid_t pid)
{
	if (ptrace(PTRACE_CONT, pid, NULL, NULL))
		err(1, "ptrace(CONT)");

	int status;
	if (waitpid(pid, &status, WUNTRACED) != pid)
		errx(1, "waitpid failed");
	if (WSTOPSIG(status) != SIGTRAP)
		errx(1, "Unexpected waitpid status");
}
