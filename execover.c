#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

int main(int argc, char **argv)
{
	if (argc < 2)
		return 1;

	for (int i = 3; i < 1024; ++i)
		close(i);

	argv++;
	execvp(*argv, argv);
	return 1;
}
